# MQTT Protocol

MQTT (Message Queuing  Telemetry Transport) is a lightweight messaging protocol that provides resource-constrained network clients with a simple way to distribute telemetry information. The protocol, which uses a publish/subscribe communication pattern, is used for machine-to-machine (M2M) communication and plays an important role in the internet of things (IoT).  

The MQTT protocol is a good choice for wireless networks that experience varying levels of latency(the time it takes for data or a request to go from the source to the destination) due to occasional bandwidth constraints or unreliable connections

**Components**



1.Publish/Subscribe  
2.Message  
3.Client  
4.Server or Broker  
5.Topic  

**Publish/Subscribe**

The first concept is the publish and subscribe system. In a publish and subscribe system, a device can publish a message on a topic, or it can be subscribed to a particular topic to receive messages.



- For example Device 1 publishes on a topic.  

- Device 2 is subscribed to the same topic as device 1 is publishing in.  

- So, device 2 receives the message 

**Publish/Subscribe Architecture**



**Message**

The data carried by the MQTT protocol across the network for the application.When an message is transported by MQTT it contains  

1.Payload data  
2.Quality of Service(QoS)  
3.Collection of properties  
4.Topic Name  

Messages are the information that you want to exchange between your devices. Whether it’s a command or data. 

**Clients**

Clients subscribe to topics to publish and receive messages.Thus subscriber and publisher are special roles of a client  

1.opens the Network Connection to the Server  
2.publishes messages that other clients might be interested in.  
3.subscribe to request messages that it is interested in receiving.  
4.unsubscribes to remove a request for messages.  
5.closes the network connection to the server. 

**Server or Broker**

A program or device that acts as an intermediary between Clients which publish Application Messages and Clients which have made Subscriptions  

1.accepts network connection from Clients.  
2.accepts Application Messages published by Clients.  
3.processes Subscribe and Unsubscribe requests from Clients.  
4.forwards Application Messages that match Client Subscriptions.  
5.closes the network connection from the Client.  



There are several brokers you can use. In our home automation projects we use the Mosquitto broker which can be installed in the Raspberry Pi. Alternatively, you can use a cloud MQTT broker.  

**Topic**

Topics are the way you register interest for incoming messages or how you specify where you want to publish the message.A topic is an endpoint to that the clients connect.It acts as the central distribution hub for publishing and subscribing messages.  

Topics are represented with strings separated by a forward slash. Each forward slash indicates a topic level. Here’s an example on how you would create a topic for a lamp in your home office:  


Note: topics are case-sensitive, which makes these two topics different:  
  

If you would like to turn on a lamp in your home office using MQTT you can imagine the following scenario  


**Architecture**



This above figure depicts MQTT architecture. As mentioned MQTT is broker based protocol. In these end devices (i.e. Clients) communicate via a broker. The broker is a server which can be installed on any machine in the cloud. There are different types of brokers such as HiveMQ, Mosquitto etc. The single client and broker can also communicate with each other.  

As MQTT runs above TCP/IP layer, it is also connection oriented protocol. The client establishes connection with the broker (i.e. Server) before the communication. MQTT is a publish-subscribe protocol. Here both client and server publish about any information (i.e. A Parameter such as temperature, humidity, event (ON/OFF) etc.) to each other using "PUBLISH" message. Any number of clients or end devices can subscribe for an event with the broker. Due to this subscription, when there is a change in any event or parameter, the broker will intimate to the subscribed clients about the change in event or parameter (i.e. Temperature, humidity, etc.).  

**Message Format**


This above figure depicts MQTT message format. It consists of a fixed message header, variable header and payload. The first two bytes are used by most of the message types. Few of the message types use variable header and payload.  

**MQTT Message QoS Level**

Besides messages, the publisher also sends QoS (Quality of Service) level, which defines the guarantee of the message delivery. There are mainly three QoS levels, such as:  

At most once: The broker will only receive the published message at most once. This level should not be used for sending important messages since there are fewer possibilities that the subscribers will receive it.  

At least once: In this level, the publisher continuously sends the message until it receives an acknowledgment from the broker. In other words, it is more important that the message is received by the subscribers than to ensure that it is received only once. This is the most commonly used QoS level.  

Exactly once: The publisher and broker work in tandem to ensure that the subscriber receives messages exactly once. This level needs some additional overhead in the form of a four-part handshake, which provides a secure authentication strategy for information delivered through network architectures. It is the safest and most guaranteed QoS level but also the slowest one. Thus, it is used only when necessary.  

**Architecture Working Operation**

  

This above figure depicts MQTT message flow between client and broker. We will take two MQTT use cases to understand the working operation of MQTT architecture.  

MQTT Use Case1: Broker wants to switch ON or OFF the light connected with remote client1  

- Initially connection is established by client1 with a broker using CONNECT and CONNACK messages.  

- Next Broker communicates with Client1 to switch ON or OFF the light interfaced with it. The messages such as PUBLISH and PUBREC are used for it.  

This use case is used to switch ON/OFF the street lights in Zigbee or LoRaWAN network. The lights are usually connected with end nodes or end devices in these wireless networks. The single Zigbee or LoRaWAN gateway controls multiple end nodes. Multiple such gateways are needed to cover the entire city.  

MQTT Use Case2: Client2 or client3 wants to update temperature/humidity status to the broker based on sensors  

- Client2  and Client3 will intimate temperature or humidity update to the broker using PUBLISH message. This information is stored in the database and will be sent to all the subscribers who have subscribed to these topics (i.e. Temperature, humidity). This information is "pushed" to all the subscribed clients of the topics.  

- If client1 has already subscribed for subscription to topics (i.e. Temperature, humidity), it will get the information from broker using PUSH operation.  

This use case is used for obtaining different types of sensing information automatically whenever there are any updates. For this purpose, different types of sensors (such as humidity sensor, temperature sensor, etc.) are interfaced with end nodes. These end nodes publish information (of any event updates) to the broker. The broker intimates changes to all the subscribed clients.  

There are two dominant data exchange protocol architectures viz. broker based and bus based.  

Wireless IoT technologies such as zigbee, LoRaWAN uses MQTT for communication between clients and router. Protocols such as AMPQ, CoAP and JMS also use broker based architecture. Protocols such as DDS, REST and XMPP use bus based architecture. 

**Working**

An MQTT session is divided into four stages: connection, authentication, communication and termination. A client starts by creating a Transmission Control Protocol/Internet Protocol (TCP/IP) connection to the broker by using either a standard port or a custom port defined by the broker's operators. When creating the connection, it is important to recognize that the server might continue an old session if it is provided with a reused client identity.  

The standard ports are 1883 for nonencrypted communication and 8883 for encrypted communication – using Secure Sockets Layer (SSL)/Transport Layer Security (TLS). During the SSL/TLS handshake, the client validates the server certificate and authenticates the server. The client may also provide a client certificate to the broker during the handshake. The broker can use this to authenticate the client. While not specifically part of the MQTT specification, it has become customary for brokers to support client authentication with SSL/TLS client-side certificates.  

Because the MQTT protocol aims to be a protocol for resource-constrained and IoT devices, SSL/TLS might not always be an option and, in some cases, might not be desired. On such occasions, authentication is presented as a cleartext username and password, which are sent by the client to the server -- this, as part of the CONNECT/CONNACK packet sequence. In addition, some brokers, especially open brokers published on the internet, will accept anonymous clients. In such cases, the username and password are simply left blank.  

MQTT is called a lightweight protocol because all its messages have a small code footprint. Each message consists of a fixed header -- 2 bytes -- an optional variable header, a message payload that is limited to 256 megabytes (MB) of information and a quality of service (QoS) level.  

During the communication phase, a client can perform publish, subscribe, unsubscribe and ping operations. The publish operation sends a binary block of data -- the content -- to a topic that is defined by the publisher.  

MQTT supports message binary large objects (BLOBs) up to 256 MB in size. The format of the content will be application-specific. Topic subscriptions are made using a SUBSCRIBE/SUBACK packet pair, and unsubscribing is similarly performed using an UNSUBSCRIBE/UNSUBACK packet pair.  

Topic strings form a natural topic tree with the use of a special delimiter character, the forward slash (/). A client can subscribe to -- and unsubscribe from -- entire branches in the topic tree with the use of special wild-card characters. There are two wild-card characters: a single-level wild-card character, the plus character (+); and a multilevel wild-card character, the hash character (#). A special topic character, the dollar character ($), excludes a topic from any root wild-card subscriptions. Typically, $ is used to transport server-specific or system messages.  

Another operation a client can perform during the communication phase is to ping the broker server using a PINGREQ/PINGRESP packet sequence. This packet sequence roughly translates to ARE YOU ALIVE/YES I AM ALIVE. This operation has no other function than to maintain a live connection and ensure the TCP connection has not been shut down by a gateway or router.  

When a publisher or subscriber wants to terminate an MQTT session, it sends a DISCONNECT message to the broker and then closes the connection. This is called a graceful shutdown because it gives the client the ability to easily reconnect by providing its client identity and resuming where it left off.  

Should the disconnect happen suddenly without time for a publisher to send a DISCONNECT message, the broker may send subscribers a message from the publisher that the broker has previously cached. The message, which is called a last will and testament, provides subscribers with instructions for what to do if the publisher dies unexpectedly.

**How to Use MQTT in IoT Projects**



Here’s the steps you should follow:  

1.Set up your Raspberry Pi. Follow our Getting Started Guide with Raspberry Pi.
(https://randomnerdtutorials.com/getting-started-with-raspberry-pi/)  

2.Enable and Connect your Raspberry Pi with SSH.  
(https://randomnerdtutorials.com/installing-raspbian-lite-enabling-and-connecting-with-ssh/)  

3.You need Node-RED installed on your Pi and Node-RED Dashboard.  
(https://randomnerdtutorials.com/getting-started-with-node-red-on-raspberry-pi/)  
and  
(https://randomnerdtutorials.com/getting-started-with-node-red-dashboard/)  

4.Install the Mosquitto broker on the Raspberry Pi.  
(https://randomnerdtutorials.com/how-to-install-mosquitto-broker-on-raspberry-pi/)  

5.Add the ESP8266 or the ESP32 to this system. You can follow the next MQTT tutorials:
ESP32 and Node-RED with MQTT – Publish and Subscribe  
(https://randomnerdtutorials.com/esp32-mqtt-publish-subscribe-arduino-ide/)  

ESP8266 and Node-RED with MQTT – Publish and Subscribe  
(https://randomnerdtutorials.com/esp8266-and-node-red-with-mqtt/)  

**Benefits or Advantages** 

Following are the benefits or advantages of MQTT protocol:  

- The MQTT protocol payload can carry any type of data such as binary, ascii text etc. The receiver need to interpret and decode as per format used by the transmitter. Hence MQTT is packet agnostic.  

- It uses packet of low size and hence can be used for low bandwidth applications.  

- It offers lower battery power consumption.  

- It is reliable protocol as it uses QoS options to provide guaranteed delivery.  

- Due to its publish/subscribe model, It is scalable.  

- It offers de-coupled design as it is easy to decouple the device and server.  

- A publishing device can send data to server at any time regardless of its state.  

**Drawbacks or Disadvantages** 

Following are the drawbacks or disadvantages of MQTT protocol:  

- MQTT uses TCP protocol which requires more processing power and more memory. TCP uses handshake protocol which requires frequent wake up and communication time intervals. This affects battery consumption. Moreover TCP connected devices tend to keep sockets open for each other which adds memory/power requirements.  

- Centralized broker limits the scalability as each client devices take up some overhead. In order to avail scalability, local broker hub is used.  

- Centralized broker can be point of failure as client connections with broker are open all the time.  

- It is not easy to implement compare to HTTP.  

- It does not support advanced features such as flow control.  

- In MQTT protocol, clients must have to support TCP/IP.  

**References** 

1.(https://en.wikipedia.org/wiki/MQTT)

2.(https://internetofthingsagenda.techtarget.com/definition/MQTT-MQ-Telemetry-Transport)

3.(https://mqtt.org/)

4.(https://randomnerdtutorials.com/what-is-mqtt-and-how-it-works/)  

5.(https://www.rfwireless-world.com/Tutorials/MQTT-tutorial.html)  


















