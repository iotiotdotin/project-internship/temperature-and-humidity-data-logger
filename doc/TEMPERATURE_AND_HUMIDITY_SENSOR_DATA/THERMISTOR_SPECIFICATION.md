
**AUTHOR :- ANURAG SINGH**


**Thermistor Specifications**


The following NTC thermistor parameters can be found in the manufacturer's data sheet.

**Resistance**


This is the thermistor resistance at the temperature specified by the manufacturer, often 25°C.

**Tolerance**


Indicates how much the resistance can vary from the specified value. Usually expressed in percent (e.g. 1%, 10%, etc). For example, if the specified resistance at 25°C for a thermistor with 10% tolerance is 10,000 ohms then the measured resistance at that temperature can range from 9,000 ohms to 11000 ohms.

**B (or Beta) constant**


A value that represents the relationship between the resistance and temperature over a specified temperature range. For example, "3380 25/50" indicates a beta constant of 3380 over a temperature range from 25°C to 50°C.

**Tolerance on Beta constants**


Beta constant tolerance in percent.

**Operating Temperature Range**


Minimum and maximum thermistor operating temperature.

**Thermal Time Constant**


When the temperature changes, the time it takes to reach 63% of the difference between the old and new temperatures.

**Thermal Dissipation Constant**


Thermistors are subject to self-heating as they pass current. This is the amount of power required to raise the thermistor temperature by 1°C. It is specified in milliwatts per degree centigrade (mW/°C). Normally, power dissipation should be kept low to prevent self-heating.

**Maximum Allowable Power**


Maximum power dissipation. It is specified in Watts (W). Exceeding this specification will cause damage to the thermistor.

**Resistance Temperature Table**


Table of resistance values and associated temperatures over the thermistors operating temperature range. Thermistors operate over a relatively limited temperature range, typically -50 to 300°C depending on type of construction and coating.


**NTC-103-R-Thermistor-NTC-K-10-10k-Ohm**


Maximum Allowable Power @ 250°C: 0.55W

Features:

Resistance @ 25°C: 10kΩ (Allowable difference of 10%)


B constant (25/50°C > 10%): 4100


Temperature coefficient @ 25°C: -4.6%/°C


Thermal dissipation factor @ 250°C: 6.5mW/°C