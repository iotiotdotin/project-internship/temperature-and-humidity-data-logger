# How to contribute

Basically each contribution can be categorized into the following categories:
- **Research**
- **Code(new features, testing, bug fixes, etc)**
- **Documentation(for Codes)**
- **Examples(use cases,How to guides,etc)**

Follow these steps to **get started:** 
1. Go to [Issue tracker](https://gitlab.com/iotiotdotin/project-internship/temperature-and-humidity-data-logger/-/issues) 
2. Pick an issue that you would like to contribute to
3. Comment in that issue saying that you are working on that issue/task by tagging the Maintainer(@kshriram67)
4. Fork the project
5. Do the task (Code/Documentation/Research)
6. Creating a merge request from your fork to the upstream project
7. Maintainer or the Domain Lead reviews the code and merges
8. Task Completed!


## Submitting changes

- Please send a **merge request** with a clear list of what you've done
- Please follow coding conventions and make sure all of your commits are atomic (one feature per commit).
- Always write a clear log message for your commits. One-line messages are fine for small changes, but bigger changes should contain detailed messages 

## Resources:

- [Getting Started with Shunya Interfaces](http://demos.iotiot.in/si/docs/diy/01-quickstart)
- [Shunya Interfaces API reference](http://demos.iotiot.in/si/docs/reference/cloud/ref-aws)
- [Docker Basics](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/dockerBasics)
- [Git Workflow](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/gitBasics)
- [Forking the project](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)
- [Merge Requests](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/mergeRequests)

## Contributors List
- [Shriram K(@kshriram67)](https://gitlab.com/kshriram67)
- [Anurag Singh(@agangwar908)](https://gitlab.com/agangwar908)
- [Shivarth Chourey(@shivarthpiyu)](https://gitlab.com/shivarthpiyu)
